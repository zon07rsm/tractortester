/*
 * TM1638.c
 *
 *  Created on: 7 ���. 2016 �.
 *      Author: BARS
 */

#include "TM1638.h"

/*
 * �������� ������ �� SPI, �������������� �����
 * ������������ ��������: ���� ������ ��� ��������
 * ������������ ��������: ���
 */
void SPI_Send(uint8_t data){
	SPI2->CR1 |= SPI_CR1_BIDIOE;

	SPI2->DR = data;
	while (!(SPI2->SR & SPI_SR_TXE));
	while ((SPI2->SR & SPI_SR_BSY));
}

/*
 * ������ ������ �� SPI, �������������� �����
 * ������������ ��������:
 * 		send - ������������ ��������
 * 		count - ���������� ���� ��� ������
 * ������������ ��������: ������ ���������� ��������
 */
SPI_Read(uint8_t send, uint8_t *data_mas, uint8_t count){

	uint8_t i = 1;

	SPI2->CR1 |= SPI_CR1_BIDIOE; // ������������ �� ��������
	SPI2->DR = send; 			 // �������� ������

	while (!(SPI2->SR & SPI_SR_TXE)); // �������� ��������� ��������
	while ((SPI2->SR & SPI_SR_BSY));

	SPI2->CR1 &= ~SPI_CR1_BIDIOE; // ������������ � ����� ������. ��� ���� ����� ���������� 8 �������� ���������

	while (!(SPI2->SR & SPI_SR_RXNE));
	while ((SPI2->SR & SPI_SR_BSY));

	data_mas[0] = SPI2->DR; // �������� ���������� ������ �� �������� SPI

	while(count-- > 1){ // ��������� ����������� ���������� ���

		SPI2->DR = 0xFF; // ������ � ������� ������ � ������ ������ �������������� �������� ������������

		while (!(SPI2->SR & SPI_SR_RXNE));
		while ((SPI2->SR & SPI_SR_BSY));

		data_mas[i++] = SPI2->DR;
	}

	SPI2->CR1 |= SPI_CR1_BIDIOE;
}

/*
 * ������������� TM1386
 * ������������ ��������: ������� 0..7
 * ������������ ��������: ���
 */
void TM1638_Init(uint8_t brightness){
	CS_L;
	SPI_Send(0x88|(brightness&0x07));
	CS_H;
}


/*
 * ���������� ����� �����������
 * ������������ ��������:
 * 		num - ����� ���������� 0..7,
 * 		state - ��������� ��������� ���������� 1/0
 * ������������ ��������: ���
 */
void LED_Control(uint8_t num, uint8_t state){
	CS_L;
	SPI_Send(0x44); // ������ �� ��������������� ������
	CS_H;

	CS_L;
	SPI_Send(0xC1+num*2);

	SPI_Send(state);
	CS_H;
}

/*
 * ���������� ������� �����������
 * ������������ ��������:
 * 		start - ����� ������� ���������� 0..7
 * 		stop - ����� ���������� ���������� 0..7
 * 		state - ��������� �����������
 * ������������ ��������: ���
 *
 * ������ ��� ������������� ����������. 1 - ���/0 - ����
 */
void MAS_LED_Control(uint8_t start, uint8_t stop, uint8_t state){
	CS_L;
	SPI_Send(0x44); // ������ �� ��������������� ������
	CS_H;

	if((start + stop) < 9){

		for(;start < stop; start ++){
			CS_L;
			SPI_Send(0xC1+start*2);

			SPI_Send(state&0x01);
			CS_H;

			state >>= 1;
		}
	}
}

/*
 * ������� ���� �����������
 * ������������ ��������: ���
 * ������������ ��������: ���
 */
void LED_Clear(){
	uint8_t i;

	for(i = 0; i < 8; i ++){ // ������ ����� �� ��� �������� ������
		CS_L;
		SPI_Send(0xC1+i*2);

		SPI_Send(0);
		CS_H;
	}
}

/*
 * ���������� ����� �����������
 * ������������ ��������:
 * 		num - ����� ���������� 0..7,
 * 		state - ��, ��� ����� �� ����������
 * ������������ ��������: ���
 */
void DISP_Control(uint8_t num, uint8_t data){
	CS_L;
	SPI_Send(0x44); // ������ �� ��������������� ������
	CS_H;

	CS_L;
	SPI_Send(0xC0+num*2);

	SPI_Send(data);
	CS_H;
}

/*
 * ���������� ������� ���������
 * ������������ ��������:
 * 		start - ����� ������� ���������� 0..7
 * 		stop - ����� ���������� ���������� 0..7
 * 		data - ������ ��������
 * ������������ ��������: ���
 */
void MAS_DISP_Control(uint8_t start, uint8_t stop, uint8_t *data){
	CS_L;
	SPI_Send(0x44); // ������ �� ��������������� ������
	CS_H;

	if((start + stop) < 9){ // ���� ��� ������ �� ���������� ��������, �� ����������

		for(;start < stop; start ++){
			CS_L;
			SPI_Send(0xC0+start*2);

			SPI_Send(*data++);
			CS_H;
		}
	}
}

/*
 * ������� ���� ���������
 * ������������ ��������: ���
 * ������������ ��������: ���
 */
void DISP_Clear(){
	uint8_t i;

	CS_L;
	SPI_Send(0x44); // Fixed address
	CS_H;

	for(i = 0; i < 8; i ++){
		CS_L;
		SPI_Send(0xC0+i*2);

		SPI_Send(0);
		CS_H;
	}
}

/*
 * ������� ���� ����������� � ���������
 * ������������ ��������: ���
 * ������������ ��������: ���
 */
void ALL_Clear(){
	uint8_t i;

	CS_L;
	SPI_Send(0x40); // ������������� ������
	CS_H;

	CS_L;
	SPI_Send(0xC0); // ��������� �����

	for(i = 0; i < 16; i ++) SPI_Send(0);

	CS_H;
}

/*
 * ��������� ��������� ������
 * ������������ ��������: ���
 * ������������ ��������: ���������� 8 ���, ������ ��� ������������� ����� ������ 1 - ������/0 - �� ������
 */
uint8_t Get_Key_State(){
	uint8_t keys = 0, key_mas[4];

	CS_L;

	SPI_Read(0x42, key_mas, 4);

	CS_H;

	keys = (key_mas[3]&0x11) << 3 | (key_mas[2]&0x11) << 2 | (key_mas[1]&0x11) << 1 | (key_mas[0]&0x11);

	return keys;
}
