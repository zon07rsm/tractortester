/*
 * TM1638.h
 *
 *  Created on: 7 ���. 2016 �.
 *      Author: BARS
 */

#ifndef TM1638_H_
#define TM1638_H_

#include "init.h"

#define CS_H (GPIOB->BSRR = GPIO_BSRR_BS12)
#define CS_L (GPIOB->BSRR = GPIO_BSRR_BR12)

void TM1638_Init(uint8_t brightness);

void LED_Control(uint8_t num, uint8_t state);
void MAS_LED_Control(uint8_t start, uint8_t stop, uint8_t state);
void LED_Clear();

void DISP_Control(uint8_t num, uint8_t data);
void MAS_DISP_Control(uint8_t start, uint8_t stop, uint8_t *data);
void DISP_Clear();

void ALL_Clear();

uint8_t Get_Key_State();


#endif /* TM1638_H_ */
