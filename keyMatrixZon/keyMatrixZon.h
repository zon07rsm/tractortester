/*
 * keyMatrixZon.h
 *
 *  Created on: 20 окт. 2022 г.
 *      Author: zon07
 */

#ifndef KEYMATRIXZON_H_
#define KEYMATRIXZON_H_

//------------------ Includes -----------------------
#include "stm32f2xx_hal.h"
//------------------ Defines -----------------------

//------------------ Typedefs -----------------------
typedef enum enumKeyMatrixStatus
{
    KEYMATRIX_ERROR,
    KEYMATRIX_OK,
    KEYMATRIX_BLOCK,
}KeyMatrixStatus;

typedef enum enumKeyMatrixButtonStatus
{
    BUTTON_OFF,
    BUTTON_ON,
    BUTTON_BLOCK,
}KeyMatrixButtonStatus;

typedef struct
{
    KeyMatrixButtonStatus key1Status ;
    KeyMatrixButtonStatus key2Status ;
    KeyMatrixButtonStatus key3Status ;
    KeyMatrixButtonStatus key4Status ;
    KeyMatrixButtonStatus key5Status ;
    KeyMatrixButtonStatus key6Status ;
    KeyMatrixButtonStatus key7Status ;
    KeyMatrixButtonStatus key8Status ;
    KeyMatrixButtonStatus key9Status ;
    KeyMatrixButtonStatus key10Status ;
    KeyMatrixButtonStatus key11Status ;
    KeyMatrixButtonStatus key12Status ;
    KeyMatrixButtonStatus key13Status ;
    KeyMatrixButtonStatus key14Status ;
    KeyMatrixButtonStatus key15Status ;
    KeyMatrixButtonStatus key16Status ;
} keyMatrix;

//------------------ Variables -----------------------



//------------------ Functions prototypes -----------------------
KeyMatrixStatus readKeyMatrix (keyMatrix* keyMatrix_p);
KeyMatrixButtonStatus readButton(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
#endif /* KEYMATRIXZON_H_ */
