/*
 * keyMatrixZon.c
 *
 *  Created on: 20 окт. 2022 г.
 *      Author: zon07
 */

//------------------ Includes -----------------------
#include "keyMatrixZon.h"
#include "cmsis_os.h"

//------------------ Defines -----------------------
#define BUTTON_ACTIVE_LEVEL     0
#define BUTTON_UNACTIVE_LEVEL   1

//--------------- Rows -----------------------------
#define KeyMatrixRow1_Pin GPIO_PIN_9
#define KeyMatrixRow2_Pin GPIO_PIN_12
#define KeyMatrixRow3_Pin GPIO_PIN_9
#define KeyMatrixRow4_Pin GPIO_PIN_1

#define KeyMatrixRow1_GPIO_Port GPIOD
#define KeyMatrixRow2_GPIO_Port GPIOG
#define KeyMatrixRow3_GPIO_Port GPIOG
#define KeyMatrixRow4_GPIO_Port GPIOE

//--------------- Columns -----------------------------
#define KeyMatrixColumn1_Pin GPIO_PIN_15
#define KeyMatrixColumn2_Pin GPIO_PIN_10
#define KeyMatrixColumn3_Pin GPIO_PIN_13
#define KeyMatrixColumn4_Pin GPIO_PIN_11

#define KeyMatrixColumn1_GPIO_Port GPIOG
#define KeyMatrixColumn2_GPIO_Port GPIOG
#define KeyMatrixColumn3_GPIO_Port GPIOG
#define KeyMatrixColumn4_GPIO_Port GPIOG

//------------------ Variables -----------------------

uint8_t buttonsDrawStatus[16] = {0};
uint8_t count = 0;
//------------------ Functions definition -----------------------
KeyMatrixStatus readKeyMatrix (keyMatrix* keyMatrix_p)
{

    HAL_GPIO_WritePin(KeyMatrixColumn1_GPIO_Port, KeyMatrixColumn1_Pin, GPIO_PIN_RESET);
    if ((buttonsDrawStatus[0] = readButton(KeyMatrixRow1_GPIO_Port, KeyMatrixRow1_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    if ((buttonsDrawStatus[4] = readButton(KeyMatrixRow2_GPIO_Port, KeyMatrixRow2_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    if ((buttonsDrawStatus[8] = readButton(KeyMatrixRow3_GPIO_Port, KeyMatrixRow3_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    if ((buttonsDrawStatus[12] = readButton(KeyMatrixRow4_GPIO_Port, KeyMatrixRow4_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    HAL_GPIO_WritePin(KeyMatrixColumn1_GPIO_Port, KeyMatrixColumn1_Pin, GPIO_PIN_SET);
    osDelay(10);
    HAL_GPIO_WritePin(KeyMatrixColumn2_GPIO_Port, KeyMatrixColumn2_Pin, GPIO_PIN_RESET);
    if ((buttonsDrawStatus[1] = readButton(KeyMatrixRow1_GPIO_Port, KeyMatrixRow1_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    if ((buttonsDrawStatus[5] = readButton(KeyMatrixRow2_GPIO_Port, KeyMatrixRow2_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    if ((buttonsDrawStatus[9] = readButton(KeyMatrixRow3_GPIO_Port, KeyMatrixRow3_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    if ((buttonsDrawStatus[13] = readButton(KeyMatrixRow4_GPIO_Port, KeyMatrixRow4_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    HAL_GPIO_WritePin(KeyMatrixColumn2_GPIO_Port, KeyMatrixColumn2_Pin, GPIO_PIN_SET);
    osDelay(10);
    HAL_GPIO_WritePin(KeyMatrixColumn3_GPIO_Port, KeyMatrixColumn3_Pin, GPIO_PIN_RESET);
    if ((buttonsDrawStatus[2] = readButton(KeyMatrixRow1_GPIO_Port, KeyMatrixRow1_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    if ((buttonsDrawStatus[6] = readButton(KeyMatrixRow2_GPIO_Port, KeyMatrixRow2_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    if ((buttonsDrawStatus[10] = readButton(KeyMatrixRow3_GPIO_Port, KeyMatrixRow3_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    if ((buttonsDrawStatus[14] = readButton(KeyMatrixRow4_GPIO_Port, KeyMatrixRow4_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    HAL_GPIO_WritePin(KeyMatrixColumn3_GPIO_Port, KeyMatrixColumn3_Pin, GPIO_PIN_SET);
    osDelay(10);
    HAL_GPIO_WritePin(KeyMatrixColumn4_GPIO_Port, KeyMatrixColumn4_Pin, GPIO_PIN_RESET);
    if ((buttonsDrawStatus[3] = readButton(KeyMatrixRow1_GPIO_Port, KeyMatrixRow1_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    if ((buttonsDrawStatus[7] = readButton(KeyMatrixRow2_GPIO_Port, KeyMatrixRow2_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    if ((buttonsDrawStatus[11] = readButton(KeyMatrixRow3_GPIO_Port, KeyMatrixRow3_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    if ((buttonsDrawStatus[15] = readButton(KeyMatrixRow4_GPIO_Port, KeyMatrixRow4_Pin)) == BUTTON_ON)
    {
        ++count;
    }
    HAL_GPIO_WritePin(KeyMatrixColumn4_GPIO_Port, KeyMatrixColumn4_Pin, GPIO_PIN_SET);
    if (count > 2)
    {
        for (uint8_t i; i <= 15; ++i)
        {
            buttonsDrawStatus[i] = BUTTON_BLOCK;
        }
        keyMatrix_p->key1Status = buttonsDrawStatus[0];
        keyMatrix_p->key2Status = buttonsDrawStatus[1];
        keyMatrix_p->key3Status = buttonsDrawStatus[2];
        keyMatrix_p->key4Status = buttonsDrawStatus[3];
        keyMatrix_p->key5Status = buttonsDrawStatus[4];
        keyMatrix_p->key6Status = buttonsDrawStatus[5];
        keyMatrix_p->key7Status = buttonsDrawStatus[6];
        keyMatrix_p->key8Status = buttonsDrawStatus[7];
        keyMatrix_p->key9Status = buttonsDrawStatus[8];
        keyMatrix_p->key10Status = buttonsDrawStatus[9];
        keyMatrix_p->key11Status = buttonsDrawStatus[10];
        keyMatrix_p->key12Status = buttonsDrawStatus[11];
        keyMatrix_p->key13Status = buttonsDrawStatus[12];
        keyMatrix_p->key14Status = buttonsDrawStatus[13];
        keyMatrix_p->key15Status = buttonsDrawStatus[14];
        keyMatrix_p->key16Status = buttonsDrawStatus[15];
        count = 0;
        return KEYMATRIX_BLOCK;
    }
    keyMatrix_p->key1Status = buttonsDrawStatus[0];
    keyMatrix_p->key2Status = buttonsDrawStatus[1];
    keyMatrix_p->key3Status = buttonsDrawStatus[2];
    keyMatrix_p->key4Status = buttonsDrawStatus[3];
    keyMatrix_p->key5Status = buttonsDrawStatus[4];
    keyMatrix_p->key6Status = buttonsDrawStatus[5];
    keyMatrix_p->key7Status = buttonsDrawStatus[6];
    keyMatrix_p->key8Status = buttonsDrawStatus[7];
    keyMatrix_p->key9Status = buttonsDrawStatus[8];
    keyMatrix_p->key10Status = buttonsDrawStatus[9];
    keyMatrix_p->key11Status = buttonsDrawStatus[10];
    keyMatrix_p->key12Status = buttonsDrawStatus[11];
    keyMatrix_p->key13Status = buttonsDrawStatus[12];
    keyMatrix_p->key14Status = buttonsDrawStatus[13];
    keyMatrix_p->key15Status = buttonsDrawStatus[14];
    keyMatrix_p->key16Status = buttonsDrawStatus[15];
    count = 0;
    return KEYMATRIX_OK;
}

KeyMatrixButtonStatus readButton(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin){
    if (BUTTON_ACTIVE_LEVEL == HAL_GPIO_ReadPin(GPIOx, GPIO_Pin))
    {
        return BUTTON_ON;
    }
    else
    {
        return BUTTON_OFF;
    }
}

