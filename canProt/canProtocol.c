/*
 * canProtocol.c
 *
 *  Created on: Oct 18, 2022
 *      Author: zon07
 */
#include "canProtocol.h"

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

CAN_TxHeaderTypeDef canMsgMFAUserCmdPosMsgTx;
CAN_TxHeaderTypeDef canMsgMFAUserCmdBtnMsgTx;
CAN_TxHeaderTypeDef canMsgETC1tx;
CAN_TxHeaderTypeDef canMsgEEC1tx;
CAN_TxHeaderTypeDef canMsgEEC2tx;
CAN_TxHeaderTypeDef canMsgEEC3tx;
CAN_TxHeaderTypeDef canMsgLFE1tx;
CAN_TxHeaderTypeDef canMsgLFCtx;
CAN_TxHeaderTypeDef canMsgHOURStx;
CAN_TxHeaderTypeDef canMsgTP_CM;
CAN_TxHeaderTypeDef canMsgTP_DT;

CAN_RxHeaderTypeDef canMsgEEC2rx;

CAN_FilterTypeDef filterMsgEEC2;

uint8_t EC1_CM[8] = {0x20, 0x22, 0x00, 0x05, 0xFF, 0xE3, 0xFE, 0x00};
uint8_t EC1_DT1[8] = {0x01, 0x50, 0x1B, 0xB8, 0x90, 0x42, 0xBD, 0xA0};
uint8_t EC1_DT2[8] = {0x02, 0x0F, 0xAD, 0xA0, 0x28, 0xCB, 0xE0, 0x15};
uint8_t EC1_DT3[8] = {0x03, 0xB1, 0x8E, 0x46, 0xFF, 0xFF, 0x3E, 0x0A};
uint8_t EC1_DT4[8] = {0x04, 0x20, 0x4E, 0x02, 0x3C, 0xE2, 0x7D, 0xCB};
uint8_t EC1_DT5[8] = {0x05, 0x8E, 0x46, 0x77, 0x00, 0xFF, 0xFF, 0xFF};

union UdataMFAUserCmdPosMsg_1_1_u
{
    dataMFAUserCmdPosMsgN_1_t dataMFAUserCmdPosMsg1_1;
    uint8_t dataMFAUserCmdPosMsg1_1_arr[8];
} UdataMFAUserCmdPosMsg_1_1;

union dataMFAUserCmdBtnMsg1_1_u
{
    dataMFAUserCmdBtnMsgN_1_t dataMFAUserCmdBtnMsg1_1;
    uint8_t dataMFAUserCmdBtnMsg1_1_arr[8];
} UdataMFAUserCmdBtnMsg1_1;

union dataEEC1_u
{
    dataEEC1_t dataEEC1;
    uint8_t dataEEC1_arr[8];
} UdataEEC1;

union dataEEC2_u
{
    dataEEC2_t dataEEC2;
    uint8_t dataEEC2_arr[8];
} UdataEEC2;

union dataEEC2_CONTROL_u
{
    dataEEC2_t dataEEC2_CONTROL;
    uint8_t dataEEC2_arr[8];
} UdataEEC2_CONTROL;

union dataEEC3_u
{
    dataEEC3_t dataEEC3;
    uint8_t dataEEC3_arr[8];
} UdataEEC3;

union dataLFE1_u
{
    dataLFE1_t dataLFE1;
    uint8_t dataLFE1_arr[8];
} UdataLFE1;

union dataLFC_u
{
    dataLFC_t dataLFC;
    uint8_t dataLFC_arr[8];
} UdataLFC;

union dataHOURS_u
{
    dataHOURS_t dataHOURS;
    uint8_t dataHOURS_arr[8];
} UdataHOURS;

union dataETC1_u
{
    dataETC1_t dataETC1;
    uint8_t dataETC1_arr[8];
} UdataETC1;


/* USER CODE END PV */


void dataMFAUserCmdPosMsgN_1_Init (dataMFAUserCmdPosMsgN_1_t *data){
    data->Pos_N_1 = 0x00;
    data->TopRightFwdSts_N_1 = 0x00;
    data->DetendPosSts_N_1 = 0x00;
    data->DetendPosSts_N_1 = 0x00;
    data->Pos_N_2 = 0x00;
    data->TopRightFwdSts_N_2 = 0x00;
    data->DetendPosSts_N_2 = 0x00;
    data->DetendPosSts_N_2 = 0x00;
    data->Pos_N_3 = 0x00;
    data->TopRightFwdSts_N_3 = 0x00;
    data->DetendPosSts_N_3 = 0x00;
    data->DetendPosSts_N_3 = 0x00;
    data->MsgSeqNum_PM_N = 0x0;
    data->CRC_PM_N = 0x0;
}

void dataMFAUserCmdBtnMsgN_1_Init (dataMFAUserCmdBtnMsgN_1_t *data){
    data->BtnStsN_1 = 0x0;
    data->BtnStsN_2 = 0x0;
    data->BtnStsN_3 = 0x0;
    data->BtnStsN_4 = 0x0;
    data->BtnStsN_5 = 0x0;
    data->BtnStsN_6 = 0x0;
    data->BtnStsN_7 = 0x0;
    data->BtnStsN_8 = 0x0;
    data->BtnStsN_9 = 0x0;
    data->BtnStsN_10 = 0x0;
    data->BtnStsN_11 = 0x0;
    data->BtnStsN_12 = 0x0;
    data->BtnStsN_13 = 0x0;
    data->BtnStsN_14 = 0x0;
    data->BtnStsN_15 = 0x0;
    data->BtnStsN_16 = 0x0;
    data->BtnStsN_17 = 0x0;
    data->BtnStsN_18 = 0x0;
    data->BtnStsN_19 = 0x0;
    data->BtnStsN_20 = 0x0;
    data->BtnStsN_21 = 0x0;
    data->BtnStsN_22 = 0x0;
    data->BtnStsN_23 = 0x0;
    data->BtnStsN_24 = 0x0;
    data->MsgSeqNum_BM_N = 0x0;
    data->CRC_BM_N = 0x0;
}

void dataETC1Init(void){
    UdataETC1.dataETC1.SPN560 = 0x02;
    UdataETC1.dataETC1.SPN573 = 0x02;
    UdataETC1.dataETC1.SPN574 = 0x02;
    UdataETC1.dataETC1.SPN4816 = 0x02;
    UdataETC1.dataETC1.SPN191 = 0x2328;
    UdataETC1.dataETC1.SPN522 = 0xFF;
    UdataETC1.dataETC1.SPN606 = 0x02;
    UdataETC1.dataETC1.SPN607 = 0x02;
    UdataETC1.dataETC1.SPN5015 = 0x02;
    UdataETC1.dataETC1.SPN161 = 0xFF;
    UdataETC1.dataETC1.SPN1482 = 0xFF;
}

void dataEEC1Init(void){
    UdataEEC1.dataEEC1.SPN899 = 0x09;
    UdataEEC1.dataEEC1.SPN4154 = 0x02;
    UdataEEC1.dataEEC1.SPN512 = 0x00;
    UdataEEC1.dataEEC1.SPN513 = 0x9A;
    UdataEEC1.dataEEC1.SPN190 = 0x0000;
    UdataEEC1.dataEEC1.SPN1483 = 0x00;
    UdataEEC1.dataEEC1.SPN1675 = 0x0F;
    UdataEEC1.dataEEC1.SPN2432 = 0x9A;
}

void dataEEC2_controlInit(void){
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN558 = 0x00;
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN559 = 0x00;
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN1437 = 0x00;
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN2970 = 0x00;
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN91 = 0x00;
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN92 = 0xAD;
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN974 = 0x00;
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN29 = 0x00;
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN2979 = 0x00;
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN5021 = 0x00;
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN5399 = 0x00;
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN5400 = 0x00;
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN3357 = 0x02;
    UdataEEC2_CONTROL.dataEEC2_CONTROL.SPN5398 = 0x00;
}

void dataEEC3Init(void){
    UdataEEC3.dataEEC3.SPN514 = 0x84;
    UdataEEC3.dataEEC3.SPN515 = 0x0050;
    UdataEEC3.dataEEC3.SPN519 = 0x00;
    UdataEEC3.dataEEC3.SPN2978 = 0x7D;
    UdataEEC3.dataEEC3.SPN3236 = 0xFFFF;
    UdataEEC3.dataEEC3.SPN3237 = 0x03;
    UdataEEC3.dataEEC3.SPN3238 = 0x00;
    UdataEEC3.dataEEC3.SPN3239 = 0x03;
    UdataEEC3.dataEEC3.SPN3240 = 0x03;
}

void dataLFE1Init(void){
    UdataLFE1.dataLFE1.SPN183 = 0x07D0;
    UdataLFE1.dataLFE1.SPN184 = 0x07D0;
    UdataLFE1.dataLFE1.SPN185 = 0xFFFF;
    UdataLFE1.dataLFE1.SPN51 = 0xFF;
    UdataLFE1.dataLFE1.SPN3673 = 0xFF;
}

void dataLFCInit(void){
    UdataLFC.dataLFC.SPN182 = 0x0;
    UdataLFC.dataLFC.SPN250 = 0x0000AB10;
}

void dataHOURSInit(void){
    UdataHOURS.dataHOURS.SPN247 = 0x0;
    UdataHOURS.dataHOURS.SPN249 = 0xFFFFFFFF;
}

void canPrInit(void)
{


      canMsgMFAUserCmdPosMsgTx.ExtId = 0x0CFF0106;
      canMsgMFAUserCmdPosMsgTx.DLC = 8;
      canMsgMFAUserCmdPosMsgTx.TransmitGlobalTime = DISABLE;
      canMsgMFAUserCmdPosMsgTx.RTR = CAN_RTR_DATA;
      canMsgMFAUserCmdPosMsgTx.IDE = CAN_ID_EXT;

      canMsgMFAUserCmdBtnMsgTx.ExtId = 0x0CFF1106;
      canMsgMFAUserCmdBtnMsgTx.DLC = 8;
      canMsgMFAUserCmdBtnMsgTx.TransmitGlobalTime = DISABLE;
      canMsgMFAUserCmdBtnMsgTx.RTR = CAN_RTR_DATA;
      canMsgMFAUserCmdBtnMsgTx.IDE = CAN_ID_EXT;

      canMsgETC1tx.ExtId = 0x0CF00203;
      canMsgETC1tx.DLC = 8;
      canMsgETC1tx.TransmitGlobalTime = DISABLE;
      canMsgETC1tx.RTR = CAN_RTR_DATA;
      canMsgETC1tx.IDE = CAN_ID_EXT;

      canMsgEEC1tx.ExtId = 0x0CF00400;
      canMsgEEC1tx.DLC = 8;
      canMsgEEC1tx.TransmitGlobalTime = DISABLE;
      canMsgEEC1tx.RTR = CAN_RTR_DATA;
      canMsgEEC1tx.IDE = CAN_ID_EXT;

      canMsgEEC2tx.ExtId = 0x0CF00300;
      canMsgEEC2tx.DLC = 8;
      canMsgEEC2tx.TransmitGlobalTime = DISABLE;
      canMsgEEC2tx.RTR = CAN_RTR_DATA;
      canMsgEEC2tx.IDE = CAN_ID_EXT;

      canMsgEEC3tx.ExtId = 0x18FEDF00;
      canMsgEEC3tx.DLC = 8;
      canMsgEEC3tx.TransmitGlobalTime = DISABLE;
      canMsgEEC3tx.RTR = CAN_RTR_DATA;
      canMsgEEC3tx.IDE = CAN_ID_EXT;

      canMsgLFE1tx.ExtId = 0x18FEF200;
      canMsgLFE1tx.DLC = 8;
      canMsgLFE1tx.TransmitGlobalTime = DISABLE;
      canMsgLFE1tx.RTR = CAN_RTR_DATA;
      canMsgLFE1tx.IDE = CAN_ID_EXT;

      canMsgLFCtx.ExtId = 0x18FEE900;
      canMsgLFCtx.DLC = 8;
      canMsgLFCtx.TransmitGlobalTime = DISABLE;
      canMsgLFCtx.RTR = CAN_RTR_DATA;
      canMsgLFCtx.IDE = CAN_ID_EXT;

      canMsgHOURStx.ExtId = 0x18FEE500;
      canMsgHOURStx.DLC = 8;
      canMsgHOURStx.TransmitGlobalTime = DISABLE;
      canMsgHOURStx.RTR = CAN_RTR_DATA;
      canMsgHOURStx.IDE = CAN_ID_EXT;

      canMsgTP_CM.ExtId = 0x18ECFF00;
      canMsgTP_CM.DLC = 8;
      canMsgTP_CM.TransmitGlobalTime = DISABLE;
      canMsgTP_CM.RTR = CAN_RTR_DATA;
      canMsgTP_CM.IDE = CAN_ID_EXT;

      canMsgTP_DT.ExtId = 0x18EBFF00;
      canMsgTP_DT.DLC = 8;
      canMsgTP_DT.TransmitGlobalTime = DISABLE;
      canMsgTP_DT.RTR = CAN_RTR_DATA;
      canMsgTP_DT.IDE = CAN_ID_EXT;
}



