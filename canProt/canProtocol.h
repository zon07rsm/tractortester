/*
 * canProtocol.h
 *
 *  Created on: Oct 18, 2022
 *      Author: zon07
 */

#ifndef INC_CANPROTOCOL_H_
#define INC_CANPROTOCOL_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f2xx_hal.h"


/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#define IDLE_RPM 1000 //1000
#define MAX_RPM 2230
#define MIN_SENSOR_VALUE 100
#define MAX_SENSOR_VALUE 4000
#define ONE_PERCENT ((MAX_SENSOR_VALUE-MIN_SENSOR_VALUE)/125)
#define SPN513_OFFSET 125

/* USER CODE END PD */
/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

#pragma pack(push, 1)
typedef struct
{
    uint16_t Pos_N_1:10;
    uint8_t TopRightFwdSts_N_1:2;
    uint8_t DownLeftFwdSts_N_1:2;
    uint8_t DetendPosSts_N_1:2;
    uint16_t Pos_N_2:10;
    uint8_t TopRightFwdSts_N_2:2;
    uint8_t DownLeftFwdSts_N_2:2;
    uint8_t DetendPosSts_N_2:2;
    uint16_t Pos_N_3:10;
    uint8_t TopRightFwdSts_N_3:2;
    uint8_t DownLeftFwdSts_N_3:2;
    uint8_t DetendPosSts_N_3:2;
    uint8_t MsgSeqNum_PM_N:4;
    uint16_t CRC_PM_N:12;
} dataMFAUserCmdPosMsgN_1_t;

typedef struct
{
    uint8_t BtnStsN_1:2;
    uint8_t BtnStsN_2:2;
    uint8_t BtnStsN_3:2;
    uint8_t BtnStsN_4:2;
    uint8_t BtnStsN_5:2;
    uint8_t BtnStsN_6:2;
    uint8_t BtnStsN_7:2;
    uint8_t BtnStsN_8:2;
    uint8_t BtnStsN_9:2;
    uint8_t BtnStsN_10:2;
    uint8_t BtnStsN_11:2;
    uint8_t BtnStsN_12:2;
    uint8_t BtnStsN_13:2;
    uint8_t BtnStsN_14:2;
    uint8_t BtnStsN_15:2;
    uint8_t BtnStsN_16:2;
    uint8_t BtnStsN_17:2;
    uint8_t BtnStsN_18:2;
    uint8_t BtnStsN_19:2;
    uint8_t BtnStsN_20:2;
    uint8_t BtnStsN_21:2;
    uint8_t BtnStsN_22:2;
    uint8_t BtnStsN_23:2;
    uint8_t BtnStsN_24:2;
    uint8_t MsgSeqNum_BM_N:4;
    uint16_t CRC_BM_N:12;
} dataMFAUserCmdBtnMsgN_1_t;

typedef struct
{
    uint8_t SPN560:2;
    uint8_t SPN573:2;
    uint8_t SPN574:2;
    uint8_t SPN4816:2;
    uint16_t SPN191;
    uint8_t SPN522;
    uint8_t SPN606:2;
    uint8_t SPN607:2;
    uint8_t SPN5015:2;
    uint8_t emp1:2;
    uint16_t SPN161;
    uint8_t SPN1482;
} dataETC1_t;

typedef struct
{
    uint8_t SPN899:4;
    uint8_t SPN4154:4;
    uint8_t SPN512;
    uint8_t SPN513;
    uint16_t SPN190;
    uint8_t SPN1483;
    uint8_t SPN1675:4;
    uint8_t SPN2432;
} dataEEC1_t;

typedef struct
{
    uint8_t SPN558:2;
    uint8_t SPN559:2;
    uint8_t SPN1437:2;
    uint8_t SPN2970:2;
    uint8_t SPN91;
    uint8_t SPN92;
    uint8_t SPN974;
    uint8_t SPN29;
    uint8_t SPN2979:2;
    uint8_t SPN5021:2;
    uint8_t SPN5399:2;
    uint8_t SPN5400:2;
    uint8_t SPN3357;
    uint8_t SPN5398;
} dataEEC2_t;

typedef struct
{
    uint8_t SPN514;
    uint16_t SPN515;
    uint8_t SPN519;
    uint8_t SPN2978;
    uint16_t SPN3236;
    uint8_t SPN3237:2;
    uint8_t SPN3238:2;
    uint8_t SPN3239:2;
    uint8_t SPN3240:2;
} dataEEC3_t;

typedef struct
{
    uint32_t SPN182;
    uint32_t SPN250;
} dataLFC_t;

typedef struct
{
    uint16_t SPN183;
    uint16_t SPN184;
    uint16_t SPN185;
    uint8_t SPN51;
    uint8_t SPN3673;
} dataLFE1_t;

typedef struct
{
    uint32_t SPN247;
    uint32_t SPN249;
} dataHOURS_t;
#pragma pack (pop)


/* USER CODE END PTD */


/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */
void dataMFAUserCmdPosMsgN_1_Init(dataMFAUserCmdPosMsgN_1_t *data);
void dataMFAUserCmdBtnMsgN_1_Init(dataMFAUserCmdBtnMsgN_1_t *data);
void dataETC1Init(void);
void dataEEC1Init(void);
void dataEEC2_controlInit(void);
void dataEEC3Init(void);
void dataLFE1Init(void);
void dataLFCInit(void);
void dataHOURSInit(void);
void canPrInit(void);
/* USER CODE END PFP */


#endif /* INC_CANPROTOCOL_H_ */
