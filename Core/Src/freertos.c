/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "canProtocol.h"
#include "keyMatrixZon.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct
{
    uint16_t adcCh3;
    uint16_t adcCh10;
    uint16_t adcCh13;
    uint16_t adcVrefInit;
} ADC1_ResultsTypeDef;

typedef struct
{
    uint16_t adcCh9;
    uint16_t adcCh15;
    uint16_t adcCh8;
} ADC3_ResultsTypeDef;

uint_fast8_t buttonSts = 0;
keyMatrix keyMatrix_1 = {BUTTON_OFF};
KeyMatrixStatus matrixStatus = KEYMATRIX_BLOCK;
HAL_StatusTypeDef statusSPI = HAL_ERROR;
static uint8_t relayStats[2] = {0, 0};
keyMatrix keyMatrixButtons = {BUTTON_OFF};
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc3;
extern DMA_HandleTypeDef hdma_adc1;
extern DMA_HandleTypeDef hdma_adc3;
extern CAN_HandleTypeDef hcan1;
extern DAC_HandleTypeDef hdac;
extern SPI_HandleTypeDef hspi1;

ADC1_ResultsTypeDef ADC1_Results = {0};
ADC3_ResultsTypeDef ADC3_Results = {0};

/* USER CODE END Variables */
/* Definitions for DefaultTask */
osThreadId_t DefaultTaskHandle;
const osThreadAttr_t DefaultTask_attributes = {
  .name = "DefaultTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for ADC1RunTask */
osThreadId_t ADC1RunTaskHandle;
const osThreadAttr_t ADC1RunTask_attributes = {
  .name = "ADC1RunTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for ADC3RunTask */
osThreadId_t ADC3RunTaskHandle;
const osThreadAttr_t ADC3RunTask_attributes = {
  .name = "ADC3RunTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for txCANTask */
osThreadId_t txCANTaskHandle;
const osThreadAttr_t txCANTask_attributes = {
  .name = "txCANTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for relayModuleTask */
osThreadId_t relayModuleTaskHandle;
const osThreadAttr_t relayModuleTask_attributes = {
  .name = "relayModuleTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for buttonQueue */
osMessageQueueId_t buttonQueueHandle;
const osMessageQueueAttr_t buttonQueue_attributes = {
  .name = "buttonQueue"
};
/* Definitions for MatrixButtonsQueue */
osMessageQueueId_t MatrixButtonsQueueHandle;
const osMessageQueueAttr_t MatrixButtonsQueue_attributes = {
  .name = "MatrixButtonsQueue"
};
/* Definitions for timer100ms */
osTimerId_t timer100msHandle;
const osTimerAttr_t timer100ms_attributes = {
  .name = "timer100ms"
};
/* Definitions for timer500ms */
osTimerId_t timer500msHandle;
const osTimerAttr_t timer500ms_attributes = {
  .name = "timer500ms"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void StartADC1RunTask(void *argument);
void StartADC3RunTask(void *argument);
void StartTxCANTask(void *argument);
void StartRelayModuleTask(void *argument);
void timer100msCallback(void *argument);
void timer500msCallback(void *argument);

extern void MX_USB_DEVICE_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* creation of timer100ms */
  timer100msHandle = osTimerNew(timer100msCallback, osTimerPeriodic, NULL, &timer100ms_attributes);

  /* creation of timer500ms */
  timer500msHandle = osTimerNew(timer500msCallback, osTimerPeriodic, NULL, &timer500ms_attributes);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of buttonQueue */
  buttonQueueHandle = osMessageQueueNew (5, sizeof(uint16_t), &buttonQueue_attributes);

  /* creation of MatrixButtonsQueue */
  MatrixButtonsQueueHandle = osMessageQueueNew (16, sizeof(keyMatrix), &MatrixButtonsQueue_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of DefaultTask */
  DefaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &DefaultTask_attributes);

  /* creation of ADC1RunTask */
  ADC1RunTaskHandle = osThreadNew(StartADC1RunTask, NULL, &ADC1RunTask_attributes);

  /* creation of ADC3RunTask */
  ADC3RunTaskHandle = osThreadNew(StartADC3RunTask, NULL, &ADC3RunTask_attributes);

  /* creation of txCANTask */
  txCANTaskHandle = osThreadNew(StartTxCANTask, NULL, &txCANTask_attributes);

  /* creation of relayModuleTask */
  relayModuleTaskHandle = osThreadNew(StartRelayModuleTask, NULL, &relayModuleTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the DefaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN StartDefaultTask */
  osTimerStart(timer500msHandle, 500);
  uint8_t buttonStatus1 = 0;
  uint8_t buttonStatus2 = 0;
  /* Infinite loop */
  for(;;)
  {
    if(!HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_0))
    {
        ++buttonStatus1;
        if(0x02 == buttonStatus1)
        {
            buttonSts |= 0x01;
            buttonStatus1 = 0;
        }
    }
    else
    {
        buttonSts &= 0x02;
        buttonStatus1 = 0;
    }
    if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13))
    {
        ++buttonStatus2;
        if(0x02 == buttonStatus2)
        {
            buttonSts |= 0x02;
            buttonStatus2 = 0;
        }
    }
    else
    {
        buttonSts &= 0x01;
        buttonStatus2 = 0;
    }
    osMessageQueuePut(buttonQueueHandle, &buttonSts, 0, 10);
    matrixStatus = readKeyMatrix(&keyMatrix_1);
    osMessageQueuePut(MatrixButtonsQueueHandle, &keyMatrix_1, 0, 10);
    osDelay(100);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartADC1RunTask */
/**
* @brief Function implementing the ADC1RunTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartADC1RunTask */
void StartADC1RunTask(void *argument)
{
  /* USER CODE BEGIN StartADC1RunTask */
    HAL_ADC_Start_DMA(&hadc1, (uint32_t*)&ADC1_Results, 4);
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
  }
  /* USER CODE END StartADC1RunTask */
}

/* USER CODE BEGIN Header_StartADC3RunTask */
/**
* @brief Function implementing the ADC3RunTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartADC3RunTask */
void StartADC3RunTask(void *argument)
{
  /* USER CODE BEGIN StartADC3RunTask */
    HAL_ADC_Start_DMA(&hadc3, (uint32_t*)&ADC3_Results, 3);
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
  }
  /* USER CODE END StartADC3RunTask */
}

/* USER CODE BEGIN Header_StartTxCANTask */
/**
* @brief Function implementing the txCANTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTxCANTask */
void StartTxCANTask(void *argument)
{
  /* USER CODE BEGIN StartTxCANTask */
    uint8_t button = 0;
    HAL_DAC_Start(&hdac, DAC_CHANNEL_2);
  /* Infinite loop */
  for(;;)
  {
      if(osMessageQueueGet(buttonQueueHandle, &button, NULL, 10) == osOK)
      {
          (button & 0x01) ? HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET) : HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET);
          (button & 0x02) ? HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET) : HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);
      }
      osDelay(100);
  }
  /* USER CODE END StartTxCANTask */
}

/* USER CODE BEGIN Header_StartRelayModuleTask */
/**
* @brief Function implementing the relayModuleTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartRelayModuleTask */
void StartRelayModuleTask(void *argument)
{
  /* USER CODE BEGIN StartRelayModuleTask */


  /* Infinite loop */
  for(;;)
  {
    if(osMessageQueueGet(MatrixButtonsQueueHandle, &keyMatrixButtons, NULL, 10) == osOK)
    {
        relayStats[0] =  keyMatrixButtons.key9Status | (keyMatrixButtons.key10Status << 1)
                        | (keyMatrixButtons.key11Status << 2) | (keyMatrixButtons.key12Status << 3)
                        | (keyMatrixButtons.key13Status << 4) | (keyMatrixButtons.key14Status << 5)
                        | (keyMatrixButtons.key15Status << 6) | (keyMatrixButtons.key16Status << 7);

        relayStats[1] =  (keyMatrixButtons.key1Status) | (keyMatrixButtons.key2Status << 1)
                        | (keyMatrixButtons.key3Status) << 2 | (keyMatrixButtons.key4Status << 3)
                        | (keyMatrixButtons.key5Status) << 4 | (keyMatrixButtons.key6Status << 5)
                        | (keyMatrixButtons.key7Status) << 6 | (keyMatrixButtons.key8Status << 7);
    }
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET);
    statusSPI = HAL_SPI_Transmit(&hspi1, &relayStats[0], 1, 10);
    statusSPI = HAL_SPI_Transmit(&hspi1, &relayStats[1], 1, 10);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_SET);
    osDelay(90);
  }
  /* USER CODE END StartRelayModuleTask */
}

/* timer100msCallback function */
void timer100msCallback(void *argument)
{
  /* USER CODE BEGIN timer100msCallback */

  /* USER CODE END timer100msCallback */
}

/* timer500msCallback function */
void timer500msCallback(void *argument)
{
  /* USER CODE BEGIN timer500msCallback */
    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_14);
  /* USER CODE END timer500msCallback */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

