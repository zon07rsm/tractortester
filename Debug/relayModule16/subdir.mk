################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../relayModule16/relayModule16.c 

OBJS += \
./relayModule16/relayModule16.o 

C_DEPS += \
./relayModule16/relayModule16.d 


# Each subdirectory must supply rules for building sources it contributes
relayModule16/%.o relayModule16/%.su: ../relayModule16/%.c relayModule16/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F207xx -c -I../Core/Inc -I../USB_DEVICE/App -I../USB_DEVICE/Target -I../Drivers/STM32F2xx_HAL_Driver/Inc -I../Drivers/STM32F2xx_HAL_Driver/Inc/Legacy -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM3 -I../Middlewares/ST/STM32_USB_Device_Library/Core/Inc -I../Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -I../Drivers/CMSIS/Device/ST/STM32F2xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/zon07/Desktop/stm32cube/tractortester/canProt" -I"C:/Users/zon07/Desktop/stm32cube/tractortester/keyMatrixZon" -I"C:/Users/zon07/Desktop/stm32cube/tractortester/TM1638" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-relayModule16

clean-relayModule16:
	-$(RM) ./relayModule16/relayModule16.d ./relayModule16/relayModule16.o ./relayModule16/relayModule16.su

.PHONY: clean-relayModule16

